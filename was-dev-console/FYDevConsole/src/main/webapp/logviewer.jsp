<!DOCTYPE HTML>
<html>
<head>
<title>Factor-y Developer Console - Logging console</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="webjars/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet"
	href="webjars/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jqc-1.12.3/dt-1.10.12/cr-1.3.2/fh-3.1.2/kt-2.1.2/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.css"/>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<link rel="shortcut icon"
	href="<%= request.getContextPath() %>/images/favicon.ico" />

</head>
<body>
	<jsp:include page="WEB-INF/jsp/nav.jsp"></jsp:include>
	<div class="container-fluid">
		<button id="btnRefreshLog" class="btn btn-default">Refresh
			log &gt;&gt;</button>
		<button id="btnClearLog" class="btn btn-danger">Clear log</button>
		<br>
		<div id="logtableContainer"></div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="webjars/jquery/1.11.2/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="webjars/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jqc-1.12.3/dt-1.10.12/cr-1.3.2/fh-3.1.2/kt-2.1.2/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.js"></script>
 
	<script type="text/javascript" >
		var logviewer = {

			loadlog : function() {

				var _self = this;

				$.ajax('api/devlog/loggers/captured/log', {
					type : "GET",
					cache : false,
				}).done(function(data, status, xhr) {
					_self.showSuccess(data);
				}).fail(function(xhr) {
					console.log(xhr);
					_self.showFailure(xhr.responseJSON);
				}).always(function() {
					$("#btnRefreshLog").val('Refresh log >>');
					$("#btnRefreshLog").prop('disabled', false);
				});

			},

			// Setup the editor
			startup : function() {

				var _self = this;

				$("#logtableContainer").hide();

				// Setup the submission code 

				$("#btnRefreshLog").on("click", function() {

					$("#btnRefreshLog").val('Refrehsing >>');
					$("#btnRefreshLog").prop('disabled', false);

					_self.loadlog();

					return false;
				});

				$("#btnClearLog").on("click", function() {

					$("#btnClearLog").val('Clearing log');
					$("#btnClearLog").prop('disabled', false);

					$.ajax('api/devlog/loggers/captured/log', {
						type : "DELETE",
						cache : false,
					}).done(function(data, status, xhr) {
						_self.loadlog();
					}).fail(function(xhr) {
						_self.showFailure(xhr.responseJSON);
					}).always(function() {
						$("#btnClearLog").val('Clear log');
						$("#btnClearLog").prop('disabled', false);
					});

					return false;
				});

			},

			showSuccess : function(data) {
				var table = "<table id=\"logtable\" class=\"table table-striped table-hover table-condensed\" width=\"100%\" ><thead><tr><th>Level</th><th>Timestamp</th><th>Message</th><th>Class / method</th></tr></thead><tbody>";
				for (var i = data.length - 1; i >= 0; i--) {
					table += "<tr><td>" + data[i].level + "</td><td>"
							+ data[i].timestamp + "</td><td>" + data[i].message
							+ "</td><td>" + data[i].sourceclass + " / "
							+ data[i].sourcemethod
					"</td>"
					"</tr>";
				}
				table += "</tbody></table>";
				
				$("#logtableContainer").html(table);
				$('#logtable').DataTable();
				$("#logtableContainer").show();

			},

			showFailure : function(data) {
				console.log(data)
			}
		}

		$(document).ready(function() {

			logviewer.startup();

		});
	</script>
</body>
</html>