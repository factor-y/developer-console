<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Factor-y Development Console for WebSphere</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="webjars/bootstrap/3.3.2/css/bootstrap.min.css">
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="webjars/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link rel="shortcut icon" href="<%= request.getContextPath() %>/images/favicon.ico" /> 
	
  </head>
 <body>
  	<jsp:include page="WEB-INF/jsp/nav.jsp"></jsp:include>
	<div class="container-fluid">  
<%

String path = request.getParameter("jndi");
if (path == null) { path = ""; }

%>
<h1>JDNI Browser</h1>
<h4><a href="?jndi=">(Root)</a> | <%= path %></h4>
<table class="table table-striped table-condensed">
<thead><tr><th>Name</th></tr></thead>
<tbody>
<%

javax.naming.InitialContext ctx = new javax.naming.InitialContext();

javax.naming.NamingEnumeration<javax.naming.NameClassPair> list =
        ctx.list(path);

while (list.hasMore()) {
  String name = list.next().getName();
  String newPath = java.net.URLEncoder.encode(
                path +
                (path.length() > 0 ? "/" : "") + name
                ,"UTF-8");
%><tr><td ><a href="?jndi=<%= newPath %>"><%= name %></a></td></tr><%
}
%>
</tbody>
</table>
</div>
	<footer>
	</footer>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="webjars/jquery/1.11.2/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="webjars/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</body>
</html>