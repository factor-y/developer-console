<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Factor-y Development Console for WebSphere</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="webjars/bootstrap/3.3.2/css/bootstrap.min.css">
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="webjars/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	
	<link rel="stylesheet" href="webjars/codemirror/4.11/lib/codemirror.css">
	<link rel="stylesheet" href="webjars/codemirror/4.11/addon/display/fullscreen.css">
	<link rel="stylesheet" href="webjars/codemirror/4.11/theme/eclipse.css">
	<link rel="stylesheet" href="css/bs-callout.css" />
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link rel="shortcut icon" href="<%= request.getContextPath() %>/images/favicon.ico" /> 

	<!-- CodeMirror -->
	<script src="webjars/codemirror/4.11/lib/codemirror.js"></script>
	<script src="webjars/codemirror/4.11/addon/display/fullscreen.js"></script>
	<script src="webjars/codemirror/4.11/addon/search/search.js"></script>
	<script src="webjars/codemirror/4.11/mode/groovy/groovy.js"></script>
	
  </head>
  <body>
	<jsp:include page="WEB-INF/jsp/nav.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
			<h1>Play with WebSphere</h1>
			<form method="post" action="api/scripting/runscript" id="scriptform" >
				<div class="form-group">
					<label>Script body</label>
					<textarea id="scriptbody" name="scriptbody" ></textarea>
				</div>
				<div class="form-group">
					<input type="submit" id="btnRunScript" class="btn btn-submit" value="Run &gt;&gt;">
				</div>
			</form>
			
			<div class="bs-callout bs-callout-success" id="script-success">
				<h4>Success</h4>
				<p>
					Output:<br/>
					<span id="success-out" class="text-info"></span><br/>
				</p>
				<p>
					Return value:<br/>
					<span id="success-value" class="text-info"></span> 
				
				</p>
			</div>
			<div class="bs-callout bs-callout-warning" id="script-failure">
				<h4>Failure</h4>
				<p>
					Your script failed with error:
					<strong><span id="fail-error" class="text-warning" ></span></strong><br/>
					<br>
					<strong>Stack trace:</strong><br/>
					<span id="fail-stack"></span> 
				</p>
			</div>
			
			</div>
 		</div>
	</div>
	<footer>
	</footer>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="webjars/jquery/1.11.2/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="webjars/bootstrap/3.3.2/js/bootstrap.min.js"></script>

	<script>
		var dconsole = {
				
				editor : null,

				// Setup the editor
				startup : function() {

					var _self = this;

					
					$("#script-success").hide();
					$("#script-failure").hide();

					this.editor = CodeMirror.fromTextArea($("#scriptbody")[0], {
						lineNumbers: true,
						theme : 'eclipse',
						fullScreen : false,
					});
					this.editor.setSize(null, 400);
					window.scripteditor = this.editor;
					
					// Setup the submission code 
					
					$("#btnRunScript").on("click", function() {
						
						$("#btnRunScript").prop('disabled', true);
						$("#btnRunScript").val('Running >>');
						
						$.ajax('api/scripting/runscript', {
							type : "POST",
							cache : false,
							// data : $("#scriptform").serialize()
							data : { scriptbody : _self.editor.getDoc().getValue() }
						}).done(function(data,status,xhr) {
							_self.showSuccess(data);
						}).fail(function(xhr) {
							console.log(xhr);
							_self.showFailure(xhr.responseJSON);
						}).always(function() {
							$("#btnRunScript").val('Run >>');
							$("#btnRunScript").prop('disabled', false);
						});
						
						return false;
					});		
					
				},
				
				showSuccess : function(data) {
					$("#success-out").html(data.out.split('\n').join('<br/>'));
					$("#success-value").html(data.value.split('\n').join('<br/>'));
					$("#script-success").show();
					$("#script-failure").hide();
				},
				
				showFailure : function(data) {
					$("#fail-error").html(data.errormessage ? data.errormessage.split('\n').join('<br/>') : "");
					$("#fail-stack").html(data.exceptionStack.split('\n').join('<br/>'));
					$("#script-success").hide();
					$("#script-failure").show();
				}
		}
	
		$(document).ready(function () {
	
			dconsole.startup();
			
		});
	</script>
  </body>
</html>