<!DOCTYPE HTML>
<html>
<head>
<title>Factor-y Developer Console - Logging console</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="webjars/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet"
	href="webjars/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jqc-1.12.3/dt-1.10.12/cr-1.3.2/fh-3.1.2/kt-2.1.2/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.css"/>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
	<jsp:include page="WEB-INF/jsp/nav.jsp"></jsp:include>
	<div class="container-fluid">
		<button id="btnRefreshLog" class="btn btn-default">List
			loggers &gt;&gt;</button>
		<br>
		<div id="logtable"></div>
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="webjars/jquery/1.11.2/jquery.min.js"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="webjars/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	
		<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jqc-1.12.3/dt-1.10.12/cr-1.3.2/fh-3.1.2/kt-2.1.2/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.js"></script>

	<script>
		var logviewer = {

			// Setup the editor
			startup : function() {

				var _self = this;

				$("#logtable").hide();

				// Setup the submission code 

				$("#btnRefreshLog").on("click", function() {

					$("#btnRefreshLog").val('Listing loggers >>');
					$("#btnRefreshLog").prop('disabled', false);

					$.ajax('api/devlog/loggers', {
						type : "GET",
						cache : false,
					}).done(function(data, status, xhr) {
						_self.showSuccess(data);
					}).fail(function(xhr) {
						console.log(xhr);
						_self.showFailure(xhr.responseJSON);
					}).always(function() {
						$("#btnRefreshLog").val('List loggers >>');
						$("#btnRefreshLog").prop('disabled', false);
					});

					return false;
				});

			},

			showSuccess : function(data) {
				var table = "<table id=\"loggerdatatable\" class=\"table table-striped table-condensed\" width=\"100%\" ><thead><tr><th>Logger</th><th>Level</th></tr></thead>";

				$.each(data, function(loggerName, level) {
					table += "<tr><td>" + loggerName + "</td><td>" + level
					"</td>"
					"</tr>";
				});

				table += "</table>";
				$("#logtable").html(table);
				$("#loggerdatatable").DataTable();
				$("#logtable").show();
				;
			},

			showFailure : function(data) {
				console.log(data)
			}
		}

		$(document).ready(function() {

			logviewer.startup();

		});
	</script>
</body>
</html>