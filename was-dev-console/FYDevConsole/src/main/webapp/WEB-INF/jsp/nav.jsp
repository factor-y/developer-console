<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<%=request.getContextPath()%>">JavaEE Console for WebSphere / <strong>Factor-y</strong>
			</a>
		</div>
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Tools <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="index.jsp">Groovy shell</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="logviewer.jsp" target="logviewer">Log
								viewer</a></li>
						<li><a href="loglevels.jsp" target="loglevels">Log level
								editor</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="jndi.jsp" target="jndi">JNDI Browser</a></li>
					</ul></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><p class="navbar-text">
						Running as:
						<%=request.getUserPrincipal() != null ? request
					.getUserPrincipal().getName() : "Anonymous"%></p></li>
			</ul>
		</div>
	</div>
</nav>