package com.factor_y.commons.console.smartlog;

import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class DeveloperLogHandler extends Handler {

	/**
	 * The storage area used for buffering the unpushed log records in memory.
	 */
	private final LogRecord[] buffer;

	private int position;

	private int numPublished;

	public DeveloperLogHandler() {
		this(500);
	}

	public DeveloperLogHandler(int size) {
		if (size <= 0)
			throw new IllegalArgumentException();

		buffer = new LogRecord[size];

	}

	public void publish(LogRecord record) {
		if (!isLoggable(record))
			return;

		buffer[position] = record;
		position = (position + 1) % buffer.length;
		numPublished = numPublished + 1;

	}

	public LogRecord[] getLogRecords() {

		final ArrayList<LogRecord> result;
		
		int i;
		
		if (numPublished < buffer.length) {
			result = new ArrayList<LogRecord>(numPublished);
		} else {
			result = new ArrayList<LogRecord>(buffer.length);
		}

		if (numPublished < buffer.length) {
			for (i = 0; i < position; i++) {
				result.add(buffer[i]);
			}
		} else {
			for (i = position; i < buffer.length; i++)
				result.add(buffer[i]);
			for (i = 0; i < position; i++) {
				result.add(buffer[i]);
			}
		}

		return result.toArray(new LogRecord[result.size()]);
	}

	@Override
	public void close() {
		for (int i = 0; i < buffer.length; i++) {
			buffer[i] = null;
		}
	}

	@Override
	public void flush() {
		position = 0;
		numPublished = 0;
	}

}
