package com.factor_y.commons.console.smartlog;

import java.util.logging.Level;
import java.util.logging.LogRecord;

public class TestLogHandler {

	public static void main(String[] args) {
		
		DeveloperLogHandler h = new DeveloperLogHandler();
		
		LogRecord lr;
		
		for (int i = 0; i < 1900; i++) {
			lr = new LogRecord(Level.INFO, "Info " + i);
			h.publish(lr);
		}

//		h.flush();

		for (int i = 0; i < 300; i++) {
			lr = new LogRecord(Level.INFO, "Info " + i);
			h.publish(lr);
		}
		
		LogRecord[] records = h.getLogRecords();
		for (LogRecord logRecord : records) {
			System.out.println(logRecord.getMillis() + " " + logRecord.getLevel() + " " + logRecord.getMessage());
		}
	}
}
