package com.factor_y.commons.console;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.LoggingMXBean;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

@Path("/devlog")
public class DeveloperLoggingFacilities {

	@Path("/loggers/captured")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCapturedLoggers() {

		return Response.ok(
				DeveloperLoggingHandlerListener.getCapturedLoggers(),
				MediaType.APPLICATION_JSON_TYPE).build();

	}

	@Path("/loggers/captured")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response captureLogger(@QueryParam("loggername") String[] loggernames) {

		for (String loggername : loggernames) {
			DeveloperLoggingHandlerListener.logToDeveloperLog(loggername);
		}

		return Response
				.ok()
				.header("api-result-message",
						"Logger " + loggernames + " added to developer capture")
				.build();
	}

	@Path("/loggers/captured")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeLogger(@QueryParam("loggername") String loggername) {
		DeveloperLoggingHandlerListener.removeFromDeveloperLog(loggername);
		return Response
				.ok()
				.header("api-result-message",
						"Logger " + loggername
								+ " removed from developer capture").build();
	}

	@Path("/loggers/captured/log")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLogEntries() {

		ObjectMapper mapper = new ObjectMapper();

		ArrayNode jsonLog = mapper.createArrayNode();

		LogRecord[] logItems = DeveloperLoggingHandlerListener
				.getDeveloperHandler().getLogRecords();

		for (LogRecord record : logItems) {

			ObjectNode logItem = jsonLog.addObject();
			logItem.put("timestamp", record.getMillis());
			logItem.put("logger", record.getLoggerName());
			logItem.put("level", record.getLevel().toString());
			if (record.getParameters() != null
					&& record.getParameters().length > 0) {
				logItem.put(
						"message",
						MessageFormat.format(record.getMessage(),
								record.getParameters()));
			} else {
				logItem.put("message", record.getMessage());
			}
			logItem.put("thread", record.getThreadID());
			logItem.put("sourceclass", record.getSourceClassName());
			logItem.put("sourcemethod", record.getSourceMethodName());
			if (record.getThrown() != null) {
				logItem.put("thrown", record.getThrown().toString());
			}
			logItem.put("sequencenumber", record.getSequenceNumber());
		}

		return Response.ok(jsonLog, MediaType.APPLICATION_JSON)
				.header("api-result-message", "Log records returned").build();
	}

	@Path("/loggers/captured/log")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response flushLogEntries() {
		DeveloperLoggingHandlerListener.getDeveloperHandler().flush();
		return Response.ok().header("api-result-message", "Log flushed")
				.build();
	}

	@Path("/loggers")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRegisteredLoggers() {

		Map<String, String> loggerMap = new TreeMap<String, String>();
		
		LoggingMXBean logMX = LogManager.getLoggingMXBean();
		
		for (String loggerName : logMX.getLoggerNames()) {
			loggerMap.put(loggerName, logMX.getLoggerLevel(loggerName));
			
		}
		
		return Response
				.ok(loggerMap,
						MediaType.APPLICATION_JSON)
				.header("api-result-message", "Loggers list with level").build();
	}

}
