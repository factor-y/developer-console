package com.factor_y.commons.console;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.factor_y.commons.console.smartlog.DeveloperLogHandler;

@WebListener
public class DeveloperLoggingHandlerListener implements ServletContextListener {

	private static DeveloperLogHandler handler;
	private static Set<String> capturedLoggers;
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
		// Clean up capture loggers
		for (String loggerName : capturedLoggers) {
			removeHandlerInternal(loggerName);
		}
		capturedLoggers.clear();
		
		// Clean up handler
		if (handler != null) {
			handler.flush();
			handler.close();
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		handler = new DeveloperLogHandler();	
		capturedLoggers = new HashSet<String>();
	}
	
	public static void logToDeveloperLog(String loggerName) {
		
		if (capturedLoggers.contains(loggerName)) {
			// Do not add twice
			// Log erratic usage
			return;
		}
		
		Logger l = Logger.getLogger(loggerName);
		l.addHandler(getDeveloperHandler());
		capturedLoggers.add(loggerName);
		
	}
	
	public static void removeFromDeveloperLog(String loggerName) {
		if (! capturedLoggers.contains(loggerName)) {
			// Log erratic usage
			return;
		}
		removeHandlerInternal(loggerName);
		capturedLoggers.remove(loggerName);
	}

	private static void removeHandlerInternal(String loggerName) {
		Logger l = Logger.getLogger(loggerName);
		l.removeHandler(getDeveloperHandler());
	}
	
	public static DeveloperLogHandler getDeveloperHandler() {
		return handler;
	}

	public static Set<String> getCapturedLoggers() {
		return capturedLoggers;
	}

}
